from rest_framework import routers
from django.urls import path
from django.conf.urls import url, include
from api.v1 import views
from articles.views import ArticleViewSet

router = routers.SimpleRouter()
router.register(r'article', ArticleViewSet, basename='article_v1')
urlpatterns = [
    path('welcome', views.welcome, name="welcome"),
    path('', views.hello, name="hello"),
    url(r'^', include(router.urls)),
]
app_name = 'v1'
