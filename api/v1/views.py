from django.http import HttpResponse

def hello(request):
    s = "Hello Guest"
    return HttpResponse(s)

def welcome(request):
    s = "Welcome to DRF"
    return HttpResponse(s)