# blog_app_api

virtualenv -p python3 venv

source venv/bin/activate

pip install -r requirements.txt

django-admin.py startproject blog

python manage.py runserver

