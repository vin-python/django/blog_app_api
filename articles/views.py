from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import status, mixins

from articles.models import Article
from articles.serializers import ArticleSerializer

def article_list(request):
    articles_data = {'name': 'Article', 'notes': 'This is  notes regarding articles'}
    return render(request, 'articles/article_list.html',  {'articlesData':articles_data})



class ArticleViewSet(viewsets.ModelViewSet):
    """
    Since we are uing ModelViewset, this is all that we need for the API view.
    All the CRUD operations are taken care of
    """
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()