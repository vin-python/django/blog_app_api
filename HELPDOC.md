https://github.com/iamshaunjp/django-playlist/tree/lesson-32
https://www.youtube.com/watch?v=TblSa29DX6I&list=PL4cUxeGkcC9ib4HsrXEYpQnTOTZE1x0uc&index=3


== Run Server: python manage.py runserver  ====

Start ENV: source bin/activate  //reactivate - work on projectname

1: django-admin.py startproject blog_app_api



2: change DB connection  => edit settings.py

        DATABASES = {
                    'default': {
                        'ENGINE': 'django.db.backends.mysql',
                        'NAME': 'djangoblog', # databasename
                        'USER': 'root',
                        'PASSWORD': 'root',
                        'HOST': 'localhost',
                        #'PORT': '8889' #[default port of mysql is 3306]

                    }
                }

3: create static page return resposne
        edit urls.py

4: render template using tempaltes

            for template render:
            edit settings.py  === 'DIRS': ['templates'],



5:  create app:
        articles - app name
        python manage.py startapp articles  (appname - pluralized app name)


6:  create urls.py inside appname i.e. articles
         url(r'^$', views.article_list)

7: create a function article_list in views.py

8: for render create a templates inside article   = for name spacing  (articles(app)/templates/articles/article_list.html

9: make an entry of newapp (articles) in main settings.py

              Edit  settings.py   under "INSTALLED_APPS"

              INSTALLED_APPS = [
                'django.contrib.admin',
                'django.contrib.auth',
                'django.contrib.contenttypes',
                'django.contrib.sessions',
                'django.contrib.messages',
                'django.contrib.staticfiles',
                'articles',
                ]

10: Default URL for articles app and root (djangoblog) app are same  ==  it's cases error 404
     soln: edit root urls.py == import module include that include url from other app
     Add new rule:
            url(r'^articles/', include('articles.urls')),  ==> it must be above default urls rule


11: django models
    https://docs.djangoproject.com/en/2.0/ref/models/fields/

    11.1: articles/models.py (working with database):  Models in jango are classes which represent a table in database

    11.2: import models

    11.3: write filed inside class

    11.4: migration of model ->
    11.4.1: python manage.py makemigrations articles (appname)
    11.4.2: python manage.py migrate articles

            (migrations file is basically tracks changes make in model )
            (a migrations folder created inside articles folder)

     11.4.3: We can add or remove field from model and do the step 11.4.1 & 11.4.2 (ALTER TABLE)

12: ORM

    12.1: python manage.py shell
    12.2: from articles.models import Article
    12.3: Article.objects.all()
    12.4: adata = Article()
          adata.title = 'Title1'
          adata.save()

13:
    python manage.py runserver

14: Django Admin

    13.1: Its useses to manage the content of Model different tables
    13.2: http://127.0.0.1:8000/admin
    13.3: python manage.py createsuperuser
    13.4: create username and password and save somewhere for later use
    13.5: Djnago Admin (handled by Django) directly connect to Database,  its like phpadmin
    13.6: It's like to CMS, we just need to register app (articles) in admin
    13.7: edit articles/admin.py
